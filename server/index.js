const app = require("express")();
const Sequelize = require("sequelize");
const mysql = require("mysql2");
const faker = require("@faker-js/faker").faker;
const { v4 } = require("uuid");

let sequelize = null;
let User = null;

(async () => {
    const host = process.env.DB_HOST || "localhost";
    const user = process.env.DB_USER || "root";
    const password = process.env.DB_PASSWORD || "root";

    const connection = mysql.createConnection({
        host,
        user,
        password,
    });

    connection.query(`CREATE DATABASE IF NOT EXISTS users_db`, async function (err, results) {
        sequelize = new Sequelize("users_db", user, password, {
            host,
            dialect: "mysql",
        });

        await sequelize.authenticate();

        User = sequelize.define("user", {
            id: {
                type: Sequelize.STRING,
                primaryKey: true,
            },
            name: Sequelize.DataTypes.STRING,
        });

        await sequelize.sync();

        console.log("CONNECTED");
    });
})();

app.get("/users", async (req, res) => {
    console.log("GET /users");

    const users = await User.findAll();

    res.json(users);
});

app.post("/users", async (req, res) => {
    console.log("POST /users");

    const user = User.build({
        id: v4(),
        name: faker.name.fullName(),
    });

    await user.save();

    res.json(user);
});

const port = process.env.PORT || 3000;

app.listen(port, () => console.log(`App is running on port ${port}`));
